<?php

namespace App\Http\Controllers;

use App\contact;
use App\Http\Resources\ContactResource;
//use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Validator;

class ContactController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|max:50|min:5|email',
                'name' => 'required|max:50|min:3',
                'subject' => 'required|max:50|min:3',
                'body' => 'required|max:500|min:5'
            ]
        );
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(
                $errors, 400);
        } else {

            $contact = new contact;
            $contact->email = request('email');
            $contact->name = request('name');
            $contact->subject = request('subject');
            $contact->body = request('body');
            $contact->save();

            return response()->json([
                'data' => 'Content created'
            ], 201);

        }
    }

    public function email_search(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|max:50|min:5|email'
            ]
        );
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(
                $errors, 400);
        } else {

            $contacts = contact::where('email', '=', $request->input('email'))->get();

            if ($contacts->first()) {
                return ContactResource::collection($contacts);
            } else {
                return response()->json([
                    'data' => 'Email not found'
                ], 404);
            }
        }

    }

    public function search(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'input' => 'required|max:500|min:3'
            ]
        );
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(
                $errors, 400);
        }
        else {
            $contacts = contact::where('email', '=', $request->input('input'))->get();
            $contacts2 = contact::where('name', '=', $request->input('input'))->get();
            $contacts3 = contact::where('subject', '=', $request->input('input'))->get();
            $contacts4 = contact::where('body', '=', $request->input('input'))->get();
            if ($contacts->first()) {

                return ContactResource::collection($contacts);
            }
            elseif ($contacts2->first()){
                return ContactResource::collection($contacts2);
            }
            elseif ($contacts3->first()){
                return ContactResource::collection($contacts3);
            }
            elseif ($contacts4->first()){
                return ContactResource::collection($contacts4);
            }
            else{
                return response()->json([
                    'data' => 'Data not found'
                ], 404);
            }
        }
    }
}

